#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

/*
k → i = h(k) % N
k' -> i = h(k') % N
arr[i] = v

X -> 9
X -> 1
X
X -> 4, 5, 8
X
X
X -> 3
X
X

h(9) = h(4) = 1
h(7) = 2
h(18) = 2

0
1 -> 9
2 -> 4
3 -> 7
4 -> ?
5 -> ?
6 -> ?
7 -> ?
*/

#define N 1024

typedef const char* Key;
typedef int Value;


typedef struct HashTableCell {
    struct HashTableCell* next;
    struct HashTableCell* prev;
    Key key;
    Value val;
} HashTableCell;

/*
Reminder: C passes arguments by copy, Python by reference:

void f1(struct Point p) {
    p.x = 4;
}
void g1(void) {
    Point p;
    p.x = 7;
    f1(p);
    // here, p.x = 7
}

// No Python equivalent


void f2(struct Point* p) {
    p->x = 4;
}
void g2(void) {
    Point p;
    p.x = 7;
    f2(&p);
    // here, p.x = 4
}

def f2(p: Point) -> None:
    p.x = 4

def g2() -> None:
    p = Point()
    p.x = 7
    f2(p)
    #  here p.x = 4
*/

typedef HashTableCell* HashTableList;
// using HashTableList = HashTableCell*;

typedef struct HashTable {
    HashTableList cells[N];
} HashTable;

/*
typedef struct Point {
    int x;
    int y;
    int z;
    char n[40];
} HashTable;

______xxxxxxxxyyyyyyyyzzzzzzzznnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn_____


typedef struct HashTable {
    int x;
    int y;
    int z;
    char* n;
} HashTable;


______xxxxxxxxyyyyyyyyzzzzzzzznnnnnnnn_____

______??????????????????????????____
      ^
      `- nnnnnnnn
*/

void ht_init(HashTable* ht);
void ht_deinit(HashTable* ht);

//HashTable* ht_new(void);
//void ht_del(HashTable* ht);

void ht_set(HashTable* ht, Key key, Value val);
bool ht_maybe_get(HashTable* ht, Key key, Value* val);


void ht_init(HashTable* ht) {
    for (int i = 0; i < N; i++) {
        ht->cells[i] = NULL;
    }
    // memset(ht->cells, 0, sizeof(ht->cells));  // for array
    // memset(ht->cells, 0, sizeof(HashTableList) * N);  // for array or pointer
}

void ht_deinit(HashTable* ht) {
    for (int i = 0; i < N; i++) {
        HashTableCell* cell = ht->cells[i];
        while (cell != NULL) {
            HashTableCell* next = cell->next;
            free(cell);
            // here, you cannot use the value of cell but you can assign cell
            cell = next;
        }
    }
}

int key_hash(Key key) {
    // TODO: this is a terrible hash function!
    int ret = 0;
    for (const char* c = key; *c != '\0'; c++) {
        ret += *c;
    }
    return ret;
}

void ht_set(HashTable* ht, Key key, Value val) {
    int i = key_hash(key) % N;
    HashTableList list = ht->cells[i];
    HashTableCell* cell = list;
    while (cell != NULL) {
        if (cell->key == key) {
            cell->val = val;
            return;
        }
        cell = cell->next;
    }

    // insert cell in front of list
    // void* malloc(size_t)
    cell = malloc(sizeof(HashTableCell));
    assert(cell);
    cell->next = list;
    cell->prev = NULL;
    cell->key = key;
    cell->val = val;

    if (list != NULL) {
        list->prev = cell;
    }

    ht->cells[i] = cell;
}

int main(void) {
    HashTable ht;
    ht_init(&ht);
    ht_set(&ht, "Bonjour", 42);
    ht_deinit(&ht);
}
